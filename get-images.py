#!/bin/python
import sys
import subprocess

# Global variables
LABEL = ""
NAMESPACE = ""

"""Setup"""


def _get_arguments():
    global LABEL
    global NAMESPACE

    if len(sys.argv) != 3:
        print("Please pass a namespace and label using the following "
              "format:\nkey=./get-images [NAMESPACE] [VALUE]=[FORMAT]")
        sys.exit(0)
    # Validate that the argument passed is string, in kubectl cmd
    NAMESPACE = sys.argv[1]
    # Validate that the argument passes is string=string, in kubectl cmd
    LABEL = sys.argv[2]


def _get_container_images(namespace, label_key_value):
    print("\nContainer images with label {}:".format(label_key_value))
    subprocess.call(
        ["kubectl", "get", "po", "--namespace={}".format(namespace), "-l",
         "{}".format(label_key_value), "-o", "jsonpath='{range .items[*]}{"
                                             ".spec.containers[*].image}{"
                                             "\"\\n\"}{end}'"
         ])


def _get_deployment_images(namespace, label_key_value):
    print("\nDeployment images with label % s:" % label_key_value)
    subprocess.call(
        ["kubectl", "get", "deploy", "--namespace={}".format(namespace), "-l",
         "% s" % label_key_value, "-o", "jsonpath='{"
                                        "..spec.containers..image}{\"\\n\"}"
         ])


def main():
    _get_arguments()
    _get_container_images(NAMESPACE, LABEL)
    _get_deployment_images(NAMESPACE, LABEL)
